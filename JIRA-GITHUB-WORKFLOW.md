# Github/JIRA Workflow

## Assumptions common to all scenarios

* Your Github organization is called MyOrg
* Your JIRA Project is called MYPROJ
* You have configured JIRA's DVCS add-on to link the MYPROJ JIRA Project to the MyOrg github organization
* The Github/JIRA synchronization process has completed (ours took several hours)

## New Issue Scenario

### Assumptions

* You are in a workspace with your component's master branch checked out.

### Steps

* Open a JIRA Issue for the work you are about to do, if it doesn't exist already. E.g  MYPROJ-1

* In your workspace, create a branch from master and name it MYPROJ-1. If you miss having descriptive, use git config to associate a description with the branch. Then the git-branches will show you the branch descriptions as part of the output. Git branch descriptions can be create by saying:

    `git config branch.MYPROJ-1.description "update turboencabulator"`

* In your git commit messages, have the first line read:

    `MYPROJ-1 #comment <whatever you want to say>`

  You can also use these tags to set the issue resolution status, add time tracking, etc.

* As you complete work (or when you are finished with the issue) push
 your branch to MyOrg/MYPROJ-1. It's fine to push to some other remote,
 like a fork, while you are working, but JIRA annotations will not
 happen, pull requests will be ignored, etc. As such, merges should
 only originate from branches in the canonical repo for the
 project. This is good practice anyway.

* When commiting the change that resolves the issue you are working on, use a commit message like this:

    `MYPROJ-1 #deliver #comment resolved issue by blah blah`

   Replace "#deliver" with whatever the _verb_ form of the appropriate
   workflow step for your project. Although the issue status will say
   "Delivered", the actual workflow steps are generally verbs. You can
   find the values for your project by clicking the Workflow button in
   the issue view.

* Generate a pull-request, using the github UI or a tool (we use hub). The first line of the pull request comment should read:

    `MYPROJ-1 #comment <the url for the issue>`

 You can put more stuff in the comment, but its handy to have a link
 to take you to the issue directly from the PR review screen.

 At this point, JIRA will have discovered your development branch after
 seeing the commit messages containing tags. JIRA will also have
 discovered the Pull Request since it originated from the branch it was
 aware of, or if there were no tagged commits, because the PR
 description includes a tag that causes the association.


## Using the issue tool

* Verify your $HOME/.jrest.cfg file. See the README.md file for an example (if someone wouldn't mind to add OSX Keychain support for this, I'd be much obliged)

* Verify that you have set the corrent jrest.project setting in gitconfig. See the README.md

* Verify the $EDITOR is set in your environment, otherwise you'll get vi whether you want it or not.

* Verify that you have "master" checked out.

* Run the issue tool:

    issue -b new Bug

* Fill in the summary and description fields in the json template. When done, save and exit your editor.

* If everything is successful, you'll find yourself in a branch named for a newly created JIRA issue.
