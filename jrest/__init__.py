import urllib2
import httplib
import os
import socket
import ssl
import json
import time
import anydbm
import logging
import sys
import base64
from datetime import datetime, date


LOGGER = logging.getLogger('jrest')
LOGGER.setLevel(logging.DEBUG)


class JiraError(Exception):
    pass


class InvalidStatus(Exception):
    pass

# See:
# https://docs.atlassian.com/jira/REST/latest/
# https://developer.atlassian.com/jiradev/api-reference/jira-rest-apis/jira-rest-api-tutorials


class JrestCli:

    def __init__(self,
                 user,
                 passwd,
                 baseuri,
                 realm="jira",
                 logger=LOGGER,
                 debuglevel=0):

        self.user = user
        self.passwd = passwd
        self.baseuri = baseuri
        self.realm = realm
        self.logger = logger
        self.cfgdir = os.path.expandvars("$HOME/.jrest")
        self.baseuri = baseuri
        self._projdata = {}
        self.sslctx = ssl.create_default_context(
            cafile="/etc/ssl/certs/ca-bundle.crt")

    def check_cachedir(self):
        if not os.path.exists(self.cfgdir):
            os.makedirs(self.cfgdir)

    def cache_fresh(self, fn):
        if os.path.exists(fn):
            if os.path.getmtime(fn) > (time.time() - 3600):
                return True
        return False

    def fetch_projects(self):
        self.logger.debug("fetching all creation metadata")
        url = "%s/rest/api/2/project" % (self.baseuri)
        req = urllib2.Request(
            url, headers={'Authorization': self.auth_header()})
        response = urllib2.urlopen(req, context=self.sslctx)
        body = response.read()
        return json.loads(body)

    def fetch_project_createmeta(self, key):
        url = "%s/rest/api/2/issue/createmeta?projectKeys=%s&expand=projects.issuetypes.fields" % (
            self.baseuri, key)
        req = urllib2.Request(
            url, headers={'Authorization': self.auth_header()})
        response = urllib2.urlopen(req, context=self.sslctx)
        body = response.read()
        return json.loads(body)

    def fetch_project_issues(self, key):
        url = "%s/rest/api/2/search?projectKeys=%s" % (
            self.baseuri, key)
        req = urllib2.Request(
            url, headers={'Authorization': self.auth_header()})
        response = urllib2.urlopen(req, context=self.sslctx)
        body = response.read()
        return json.loads(body)

    def versions(self, key):
        d = self.fetch_project_createmeta(key)
        versions = d['projects'][0]['issuetypes'][0]['fields']['versions']['allowedValues']
        fmt = "%Y-%m-%d"
        today = date.today().strftime(fmt)
        return [(datetime.strptime(x.get('releaseDate', today), fmt),
                 x['name'],
                 x['id']) for x in versions]

    def get_issue_transition_ids(self, issueid):
        url = "%s/rest/api/2/issue/%s/transitions?expand=transitions.fields " % (
            self.baseuri, issueid)
        req = urllib2.Request(
            url, headers={'Authorization': self.auth_header()})
        response = urllib2.urlopen(req, context=self.sslctx)
        return {x["name"]: x["id"] for x in json.load(response)['transitions']}

    def set_issue_status(self, issueid, status):
        transids = self.get_issue_transition_ids(issueid)
        if status not in transids:
            raise InvalidStatus(status)

        body = {"transition": {"id": transids[status]}}
        url = "%s/rest/api/2/issue/%s/transitions" % (
            self.baseuri, issueid)
        req = urllib2.Request(
            url, headers={
                'Content-Type': 'application/json',
                'Authorization': self.auth_header(),
            })
        response = urllib2.urlopen(req,
                                   context=self.sslctx,
                                   data=json.dumps(body))
        body = response.read()
        return json.loads(body)

    def projdata(self, name):
        if name not in self._projdata:
            metadata = self.fetch_project_createmeta(name)
            assert len(metadata['projects']) is 1
            project = metadata['projects'][0]
            pdata = {'id': project['id'],
                     'issuetypes': {}}
            for d in project['issuetypes']:
                pdata['issuetypes'][str(d[u'name'])] = str(d[u'id'])
            self._projdata[name] = pdata
        return self._projdata[name]

    def projectid(self, name):
        pdata = self.projdata(name)
        return pdata['id']

    def project_issuetypes(self, name):
        pdata = self.projdata(name)
        return pdata['issuetypes'].keys()

    def auth_header(self):
        return "Basic %s" % base64.encodestring('%s:%s' % (self.user, self.passwd)).replace('\n', '')

    def post(self, data):
        url = "%s/rest/api/2/issue/" % self.baseuri
        try:
            req = urllib2.Request(url,
                                  data=json.dumps(data),
                                  headers={'Content-Type': 'application/json',
                                           'Authorization': self.auth_header()})
            res = urllib2.urlopen(req, context=self.sslctx)
            return res
        except urllib2.HTTPError as e:
            error_message = e.read()
            self.logger.debug("error: %s", error_message)
            raise JiraError(error_message)
