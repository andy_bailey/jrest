import jrest
import os
import sys
import argparse
import ConfigParser
import tempfile
import subprocess
import logging
import json
from heapq import nsmallest
import datetime

logger = logging.getLogger('jrest')
logger.setLevel(logging.DEBUG)
sh = logging.StreamHandler(sys.stderr)
sh.setLevel(logging.DEBUG)
logger.addHandler(sh)


def placeholder(field):
    return "<Enter %s here>" % field.capitalize()


def default_template(project):
    return {"fields":
            {
                "project": {"key": project},
                "summary": placeholder('summary'),
                "description": placeholder('description'),
                "environment": placeholder('environment')
            }
            }


def merge_inferred_versions(template, versions):
    template['fields']["versions"] = [{"id": x[2], "name": x[1]} for x in versions]
    return template


def merge_bug_template(template):
    template['fields']['issuetype'] = {"name": "Bug"}
    return template


def merge_task_template(template):
    template['fields']["issuetype"] = {"name": "Task"}
    return template


def merge_improvement_template(template):
    template['fields']["issuetype"] = {"name": "Improvement"}
    return template


def gen_bug(project, versions):
    template = tempfile.NamedTemporaryFile(delete=False)
    issue = merge_inferred_versions(
        merge_bug_template(default_template(project)), versions)
    json.dump(issue, template, indent=4)
    template.flush()
    return template.name


def gen_task(project, *args):
    template = tempfile.NamedTemporaryFile(delete=False)
    issue = merge_task_template(default_template(project))
    json.dump(issue, template, indent=4)
    template.flush()
    return template.name


def gen_improvement(project, *args):
    template = tempfile.NamedTemporaryFile(delete=False)
    issue = merge_improvement_template(default_template(project))
    json.dump(issue, template, indent=4)
    template.flush()
    return template.name


def validate_issue(issue, project, jcli):
    if 'fields' not in issue:
        logger.error("no fields element in issue")
        return False

    fields = issue['fields']
    for i in ('issuetype', 'summary', 'description'):
        if i not in fields:
            logger.error("no %s element in fields", i)
            return False

    for field in ('summary', 'description'):
        if fields[field] == placeholder(field):
            logger.error("%s field unchanged from template", field)
            return False

    itype = fields['issuetype']
    if 'name' not in itype:
        name = itype['name']
        allowed = jcli.project_issuetypes(project)
        if name not in allowed:
            logger.error("issue type %s is invalid for project %s. Valid types are %s", name, project, ",".join(allowed))
            return False
    return True


def edit_template(project, issuetype, versions, jcli):
    tmplfuns = {'Bug': gen_bug,
                'Task': gen_task,
                'Improvement': gen_improvement
                }

    template = tmplfuns[issuetype](project, versions)

    editor = os.getenv('EDITOR', 'vi')
    while True:
        subprocess.call("%s %s" % (editor, template), shell=True)
        try:
            with open(template, 'r') as t:
                issue = json.load(t)
                if validate_issue(issue, project, jcli):
                    return issue
                raise Exception("invalid issue")
        except Exception:
            sys.stderr.write("syntax error in issue, re-edit? [Y/n]: ")
            r = sys.stdin.readline()
            if r.lower().startswith("n"):
                sys.exit(1)


def create_issue_branch(issue_id, summary):
    try:
        subprocess.check_output(
            "git checkout -b %s" % issue_id, shell=True, stderr=subprocess.STDOUT)
        subprocess.check_output("git config branch.%s.description %s" % (
            issue_id, issue_id), shell=True, stderr=subprocess.STDOUT)
    except Exception, x:
        logger.debug("error creating issue branch: %s", str(x))


def curbranch():
    try:
        r = subprocess.check_output(
            ["git", "rev-parse", "--abbrev-ref", "HEAD"])
        return r.rstrip("\n")
    except subprocess.CalledProcessError:
        return None


def gitconfig(key):
    try:
        r = subprocess.check_output(["git", "config", key])
        return r.rstrip("\n")
    except subprocess.CalledProcessError:
        return None


def curbranch():
    try:
        r = subprocess.check_output(
            ["git", "rev-parse", "--abbrev-ref", "HEAD"])
        return r.rstrip("\n")
    except subprocess.CalledProcessError, x:
        return None


def create_issue_branch(issue_id, summary):
    try:
        r = subprocess.check_output(
            "git checkout -b %s" % issue_id, shell=True, stderr=subprocess.STDOUT)
        r = subprocess.check_output("git config branch.%s.description %s" % (
            issue_id, summary), shell=True, stderr=subprocess.STDOUT)
    except subprocess.CalledProcessError, x:
        return None


def n_nearest_versions(jc, project, number=5):
    now = int(datetime.datetime.now().strftime("%s"))
    s = [(int(x[0].strftime("%s")), x) for x in jc.versions(project)]
    nearest = nsmallest(number, s, key=lambda x: abs(x[0] - now))
    return [x[1] for x in sorted(nearest, key=lambda x: x[0])]


def main():
    defaults = {}
    conf_parser = argparse.ArgumentParser(
        description=__doc__,
        formatter_class=argparse.RawDescriptionHelpFormatter,
        add_help=False)

    conf_parser.add_argument("-c", "--conf_file",
                             help="Specify config file",
                             metavar="FILE")
    args, remaining_argv = conf_parser.parse_known_args()

    config = ConfigParser.SafeConfigParser()
    conffiles = [".jrest.cfg", os.path.expandvars("$HOME/.jrest.cfg")]
    if args.conf_file:
        conffiles.insert(args.conf_file)

    config.read(conffiles)

    defaults = dict(config.items("jrest"))
    defaults.setdefault('project', gitconfig('jrest.project'))

    parser = argparse.ArgumentParser(parents=[conf_parser])
    parser.set_defaults(**defaults)
    parser.add_argument("-U", "--baseuri")

    subparsers = parser.add_subparsers(dest="cmd", help='help for subcommand')
    parser_a = subparsers.add_parser('list', help='list objects')
    parser_a.add_argument("object", choices=["projects", "versions", "issues"], default="project")

    parser_b = subparsers.add_parser('new', help='create a new something')
    parser_b.add_argument("issuetype", choices=["Bug", "Task", "Improvement"])
    parser_b.add_argument(
        "-b", "--branch", action="store_true", help="create a branch named for the issue id")
    parser_b.add_argument(
        "-m", "--master", action="store_true", help="assume current branch is master")

    parser_set = subparsers.add_parser('set', help='list objects')
    parser_set.add_argument("issue")
    parser_set.add_argument("-s", "--status", required=False)

    args = parser.parse_args(remaining_argv)
    jc = jrest.JrestCli(user=args.user, passwd=args.password, baseuri=args.baseuri)

    if args.project is None:
        logger.error(
            "project not set in .jrest.cfg, ~/.jrest.cfg, or .git/config")
        logger.error(
            "e.g. 'git config --add jrest.project CX'")
        sys.exit(1)

    if 'issue' in args:
        if args.status:
            print jc.set_issue_status(args.issue, args.status.replace("_", " "))
        else:
            print jc.get_issue_transition_ids(args.issue)
        sys.exit(1)

    if 'object' in args:
        if args.object == "versions":
            colfmt = "{:<9} {:>6} {:<22}"
            for (dt, name, _id) in sorted(jc.versions(args.project), key=lambda x: x[0]):
                print(colfmt.format(dt.strftime("%Y-%m-%d"), _id, name))
        elif args.object == "issues":
            colfmt = "{:<9} {:>6} {:<22}"
            for (dt, name, _id) in sorted(jc.issues(args.project), key=lambda x: x[0]):
                print(colfmt.format(dt.strftime("%Y-%m-%d"), _id, name))
        sys.exit(1)

    if args.branch and not args.master:
        branch = curbranch()
        if branch != "master":
            logger.error(
                "not branching from current, non-master branch %s", branch)
            sys.exit(1)

    projid = jc.projectid(args.project)
    if projid is None:
        logger.error(
            "project %s not found, if this project is new try rerunning with --refresh" % args.project)

    issuetypes = jc.project_issuetypes(args.project)
    if args.issuetype not in issuetypes:
        logger.error("project %s allowed issuetypes are %s" %
                     (args.project, ", ".join(issuetypes)))
        sys.exit(1)

    nearest_versions = n_nearest_versions(jc, args.project, 5)

    issue = edit_template(project=args.project,
                          issuetype=args.issuetype,
                          versions=nearest_versions,
                          jcli=jc)

    try:
        res = jc.post(issue)
        body = res.read()
        jissue = json.loads(body)
        jc.set_issue_status(str(jissue[u'key']), "In Progress")
        if args.branch:
            create_issue_branch(str(jissue[u'key']), issue['fields']['summary'])

    except jrest.JiraError, x:
        logger.error(x)
        #jerr = json.loads(x)
        #logger.error(json.dumps(jerr, indent=4))

if __name__ == "__main__":
    main()
