#!/usr/bin/env python
from setuptools import setup, find_packages
from codecs import open
from os import path
import sys
from setuptools.command.test import test as TestCommand

class PyTest(TestCommand):
    user_options = [('pytest-args=', 'a', "Arguments to pass to py.test")]

    def initialize_options(self):
        TestCommand.initialize_options(self)
        self.pytest_args = []

    def finalize_options(self):
        TestCommand.finalize_options(self)
        self.test_args = []
        self.test_suite = True

    def run_tests(self):
        #import here, cause outside the eggs aren't loaded
        import pytest
        errno = pytest.main(self.pytest_args)
        sys.exit(errno)

here = path.abspath(path.dirname(__file__))

with open(path.join(here, 'DESCRIPTION.rst'), encoding='utf-8') as f:
    long_description = f.read()

setup(
    name='jrest',
    version='1.0',
    description='shitty jira command line tool',
    long_description=long_description,
    url="https://github.com/GooseYArd/jrest.git",
    author='Andy Bailey',
    author_email='gooseyard@gmail.com',
    license='MIT',
    packages=find_packages(exclude=['venv']),
    tests_require=['pytest'],
    cmdclass = {'test' : PyTest },
    entry_points = {
        'console_scripts':
        [
            'issue=jrest.main:main',
        ],
    }
)
