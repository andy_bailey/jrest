Create a .jrest.cfg file

    cat <<EOF>~/.jrest.cfg
    [jrest]
    user=$USER
    password=<YOUR PASSWORD>
    EOF

Somebody will hopefully add Keychain support here so that we don't
have to do this. Or add oauth support to the tool.

In each project you want to use the tool with:

    git config jrest.project <JIRA PROJECT NAME>

To open an issue:

    jrest new <Issuetype>

where Issuetype is defined for your project, Bug, Task etc.

To open an issue and create a git branch simultaneously

    jrest new <Issuetype> -b
